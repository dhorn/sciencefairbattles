package com.sciencefair.main;

import java.util.LinkedList;

import org.newdawn.slick.*;

public class Controller extends BasicGame
{
	public static void main(String args[])
	{
		try
		{
			Controller controller = new Controller("Sciencefair Battles");
			AppGameContainer container = new AppGameContainer(controller);

			// container.setDisplayMode(1920, 1080, false);
			container.setDisplayMode(container.getScreenWidth(), container.getScreenHeight(), true);
			container.setAlwaysRender(true);
			container.setUpdateOnlyWhenVisible(false);
			container.setMouseGrabbed(false);
			container.start();
		}
		catch (SlickException e)
		{
			System.err.println("Unable to create and initialize Game/Container. Stack trace: ");
			e.printStackTrace();
		}
	}

	private boolean leftSideHasTurn;
	private boolean blinkEffect;
	private boolean leftSideQuestioning;
	private boolean haltTimer;
	private int blinkTimer = 0;
	private int timerMinutes;
	private int timerSeconds;
	private int timerMilliseconds;
	private float scaleFactor = 3.0f;
	private SpriteSheet spriteSheet;
	private Sound bellSound;

	public Controller(String title)
	{
		super(title);

		this.timerMinutes = 2;
		this.timerSeconds = 30;
		this.timerMilliseconds = 0;
		this.blinkEffect = true;
		this.leftSideHasTurn = false;
		this.leftSideQuestioning = true;
		this.haltTimer = true;
	}

	private void renderDigit(GameContainer gc, Graphics graphics, int number, int posX, int posY)
	{
		graphics.pushTransform();
		switch (number)
		{
		case 0:
			this.spriteSheet.getSprite(0, 0).draw(posX, posY, this.scaleFactor);
			break;
		case 1:
			this.spriteSheet.getSprite(1, 0).draw(posX, posY, this.scaleFactor);
			break;
		case 2:
			this.spriteSheet.getSprite(2, 0).draw(posX, posY, this.scaleFactor);
			break;
		case 3:
			this.spriteSheet.getSprite(3, 0).draw(posX, posY, this.scaleFactor);
			break;
		case 4:
			this.spriteSheet.getSprite(0, 1).draw(posX, posY, this.scaleFactor);
			break;
		case 5:
			this.spriteSheet.getSprite(1, 1).draw(posX, posY, this.scaleFactor);
			break;
		case 6:
			this.spriteSheet.getSprite(2, 1).draw(posX, posY, this.scaleFactor);
			break;
		case 7:
			this.spriteSheet.getSprite(3, 1).draw(posX, posY, this.scaleFactor);
			break;
		case 8:
			this.spriteSheet.getSprite(0, 2).draw(posX, posY, this.scaleFactor);
			break;
		case 9:
			this.spriteSheet.getSprite(1, 2).draw(posX, posY, this.scaleFactor);
			break;
		case 10:
			this.spriteSheet.getSprite(2, 2).draw(posX, posY, this.scaleFactor);
			break;
		case 11:
			this.spriteSheet.getSprite(3, 2).draw(posX, posY, this.scaleFactor);
			break;
		case 12:
			this.spriteSheet.getSprite(0, 3).draw(posX, posY, this.scaleFactor);
			break;
		default:
			System.err.println("Unrecognized Digit");
			break;
		}
		graphics.popTransform();
	}

	public void render(GameContainer gc, Graphics graphics) throws SlickException
	{
		if (leftSideHasTurn)
		{
			graphics.setColor(new Color(0.4f, 0.0f, 0.0f));
			graphics.fillRect(gc.getWidth() / 2, 0, gc.getWidth(), gc.getHeight());
			if (!this.haltTimer)
			{
				graphics.setColor(new Color(0.0f, 0.4f, 0.0f));
			}
			graphics.fillRect(0, 0, gc.getWidth() / 2, gc.getHeight());
		}
		else
		{
			graphics.setColor(new Color(0.4f, 0.0f, 0.0f));
			graphics.fillRect(0, 0, gc.getWidth() / 2, gc.getHeight());
			if (!this.haltTimer)
			{
				graphics.setColor(new Color(0.0f, 0.4f, 0.0f));
			}
			graphics.fillRect(gc.getWidth() / 2, 0, gc.getWidth(), gc.getHeight());
		}

		if (this.blinkEffect)
		{
			// Define some variables
			int posX = gc.getWidth() / 2 - (int) (2 * 128.0 * this.scaleFactor);
			int posY = gc.getHeight() / 2 - (int) (256.0 * this.scaleFactor / 2.0);
			int seconds = this.timerSeconds;

			// Render minute digit
			this.renderDigit(gc, graphics, this.timerMinutes, posX, posY);
			posX += 128 * this.scaleFactor;

			// Render ":"
			this.renderDigit(gc, graphics, 12, posX, posY);
			posX += 128 * this.scaleFactor;

			// Get Seconds Digits
			LinkedList<Integer> secondsStack = new LinkedList<Integer>();
			while (seconds > 0)
			{
				secondsStack.push(seconds % 10);
				seconds = seconds / 10;
			}

			if (secondsStack.size() > 1)
			{
				while (!secondsStack.isEmpty())
				{
					// Render minute digit
					this.renderDigit(gc, graphics, secondsStack.pop(), posX, posY);
					posX += 128 * this.scaleFactor;
				}
			}
			else
			{
				this.renderDigit(gc, graphics, 0, posX, posY);
				posX += 128 * this.scaleFactor;
				this.renderDigit(gc, graphics, this.timerSeconds, posX, posY);
			}
		}
	}

	public void init(GameContainer gc) throws SlickException
	{
		if (gc.getWidth() < 1920)
		{
			this.scaleFactor = 2.0f;
		}

		gc.setShowFPS(false);
		gc.setTargetFrameRate(60);
		gc.setVSync(true);
		gc.setMinimumLogicUpdateInterval(16);
		try
		{
			this.spriteSheet = new SpriteSheet(new Image("assets/Numbers.png"), 128, 256);
		}
		catch (SlickException e)
		{
			e.printStackTrace();
		}
		
		this.bellSound = new Sound("assets/sounds/bell.wav");
	}

	public void update(GameContainer gc, int delta) throws SlickException
	{
		if (gc.getInput().isMousePressed(Input.MOUSE_LEFT_BUTTON) || gc.getInput().isKeyPressed(Input.KEY_SPACE))
		{
			if (haltTimer == true) {
				this.bellSound.play();
			}
			this.haltTimer = false;
			this.switchSides();
		}

		// check for Input
		if (gc.getInput().isKeyPressed(Input.KEY_F))
		{
			// Toggle fullscreen
			gc.setFullscreen(!gc.isFullscreen());

			// Disable mouse grab if we're not in fullscreen
			gc.setMouseGrabbed(gc.isFullscreen());
		}

		blinkTimer++;
		if (this.timerSeconds > 0 || this.timerMinutes > 0)
		{
			// if (this.leftSideQuestioning == this.leftSideHasTurn)
			// {
			if (!this.haltTimer)
			{
				this.timerMilliseconds -= delta;
				while (this.timerMilliseconds < 0)
				{
					this.timerMilliseconds += 1000;
					this.timerSeconds--;
					if (this.timerSeconds < 0)
					{
						this.timerSeconds += 60;
						this.timerMinutes--;
					}
				}
			}
			// }
		}
		else if (blinkTimer >= 15)
		{
			blinkTimer = 0;
			this.blinkEffect = this.blinkEffect ? false : true;
		}
	}

	public void switchSides()
	{
		if (this.timerSeconds > 0 || this.timerMinutes > 0)
		{
			this.leftSideHasTurn = this.leftSideHasTurn ? false : true;
		}
		else
		{
			this.leftSideQuestioning = !this.leftSideQuestioning;
			this.leftSideHasTurn = !this.leftSideQuestioning;
			this.timerMilliseconds = 0;
			this.timerMinutes = 2;
			this.timerSeconds = 30;
			this.blinkEffect = true;
			this.haltTimer = true;
			this.blinkTimer = 0;
		}
	}
}
